/**
* @abstract Decode GSM message to ASCII
* @author VIZCREATIONS
*/

#include <stdio.h>
#include <string.h>

const char HexTbl[] = { "0123456789ABCDEF" };

void parse(char *encd, char *decd, int len) {
}

unsigned char HexChar2Number(char hex) {
	unsigned char value = 0;
	if(hex >= '0' && hex <= '9')
		value = hex - '0';
	else if(hex >= 'A' && hex <= 'Z')
		value = hex-'A'+10;
	else if(hex >= 'a' && hex <= 'z')
		value = hex-'a'+10;

	return value;
}

unsigned char strHex2Byte(char *pHex) {
	unsigned char value;
	value = HexChar2Number(pHex[0])<<4;
	value |= HexChar2Number(pHex[1]);
	return value;
}

// Encode
int PDU_7BIT_Encoding(unsigned char *pDst, char *pSrc) {
	int i;
	unsigned char hexValue;
	unsigned char nLeft;
	unsigned char fillValue;
	int Cnt = 0;
	int nSrcLength = strlen(pSrc);

	nLeft = 1;

	for(i=0; i<nSrcLength; i++) {
		hexValue = *pSrc >> (nLeft - 1);
		fillValue = *(pSrc+1)<<(8-nLeft);
		hexValue = hexValue | fillValue;

		*pDst++ = HexTbl[hexValue>>4];
		*pDst++ = HexTbl[hexValue & 0x0F];
		Cnt += 2;

		nLeft++;
		if(nLeft == 8) {
			pSrc++;
			i++;
			nLeft = 1;
		}

		pSrc++;
	}

	*pDst = '\0';
	return Cnt;
}

// Decode
int PDU_7BIT_Decoding(char *pDst, char *pSrc) {
	int i;
	int Cnt = 0;
	unsigned char nLeft = 1;
	unsigned char fillValue = 0;
	unsigned char oldFillValue = 0;

	int srcLength = strlen(pSrc);
	for(i=0; i<srcLength; i++) {
		*pDst = strHex2Byte(pSrc);

		fillValue = (unsigned char) *pDst;
		fillValue >>= (8-nLeft);

		*pDst <<= (nLeft-1);
		*pDst &= 0x7F;
		*pDst |= oldFillValue;

		oldFillValue = fillValue;

		pDst++;
		Cnt++;

		nLeft++;
		if(nLeft==8) {
			*pDst = oldFillValue;
			pDst++;
			Cnt++;

			nLeft = 1;
			oldFillValue = 0;
		}
		pSrc += 2;
	}
	*pDst = '\0';
	return Cnt;
}

int main(int argc, char *argv[]) {
	char a = 'a';
	char gsm[20];
	char asc[20];
	int cnt;

	printf("Welcome to GSM parser\n");
	printf("a in number: %d\n", a);

	if(argc != 2) {
		printf("Usage: %s <gsm-code>\n", argv[0]);
		return -1;
	}
//	fgets(gsm, sizeof(gsm), stdin);
	if(strlen(argv[1])>0) {
		strcpy(gsm, argv[1]);

		cnt = PDU_7BIT_Decoding(asc, gsm);
		printf("GSM: %s; Decoded: %s\n", gsm, asc);
	} else puts("Error!");
	return 0;
}
