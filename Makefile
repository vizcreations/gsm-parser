# Makefile for GSM parse Linux program

CIL_INCLUDES = -I/usr/include -I. -I/usr/local/include
CIL_LIBS = -L/usr/lib -L/usr/local/lib
BIN = gsmp

default:
	gcc -o $(BIN) main.c

clean:
	$(RM) *.o gsmp tmp/
